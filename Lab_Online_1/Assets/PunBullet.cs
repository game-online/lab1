﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
[RequireComponent (typeof (PhotonTransformView))]
[RequireComponent (typeof (PhotonRigidbodyView))]
public class PunBullet : MonoBehaviourPunCallbacks {
    private void OnCollisionEnter (Collision collision) {
        PhotonNetwork.Destroy (this.gameObject);
        if (!photonView.IsMine)
            return;
    }
    //void Update()
    //{

    // }
}