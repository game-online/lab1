﻿using Photon.Pun;
using UnityEngine;
public class PunHealth : MonoBehaviourPunCallbacks {
    public const int maxHealth = 100;
    public int currentHealth = maxHealth;
    public void OnGUI () {
        if (photonView.IsMine)
            GUI.Label (new Rect (0, 0, 300, 50), "Player Health : " + currentHealth);
    }
    public void TakeDamage (int amount, int OwnerNetID) {
        if (photonView != null)
            photonView.RPC ("PunRPCTakedDamage", RpcTarget.All, amount, OwnerNetID);
        else print ("photonView is NULL.");
    }

    [PunRPC]
    public void PunRPCTakedDamage (int amount, int OwnerNetID) {
        Debug.Log ("Take Damage");
        currentHealth -= amount;
        if (currentHealth <= 0) {
            Debug.Log ("BulletID : " + OwnerNetID.ToString () + " Killed" +
                photonView.ViewID);
            currentHealth = maxHealth;
            if (photonView.IsMine) {
                photonView.RPC ("Respawn", RpcTarget.All, 10, 10, 10);
            }
        }
    }
    public void Respawn (int x, int y, int z) {
        this.GetComponent<Rigidbody> ().transform.position = new Vector3 (x, y, z);
    }
}