﻿using Photon.Pun;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
[RequireComponent (typeof (PhotonTransformView))]
public class PunUserNetControl : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback {
    [Tooltip ("The local player instance. Use this to know if the local player isrepresented in the Scene")]
    public static GameObject LocalPlayerInstance;
    public void OnPhotonInstantiate (PhotonMessageInfo info) {

        Debug.Log (info.photonView.Owner.ToString ());
        Debug.Log (info.photonView.ViewID.ToString ());

        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
        if (photonView.IsMine) {
            LocalPlayerInstance = gameObject;
            GetComponent<MeshRenderer> ().material.color = Color.blue;
        } else {
            GetComponentInChildren<Camera> ().enabled = false;
            GetComponentInChildren<AudioListener> ().enabled = false;
            GetComponent<FirstPersonController> ().enabled = false;
        }
    }
}