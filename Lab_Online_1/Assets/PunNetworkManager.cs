﻿using Photon.Pun;
using Photon.Pun.UtilityScripts;
using UnityEngine;
public class PunNetworkManager : ConnectAndJoinRandom {
    public static PunNetworkManager singleton;
    [Header ("Spawn Info")]
    [Tooltip ("The prefab to use for representing the player")]
    public GameObject GamePlayerPrefab;
    private void Awake () {
        singleton = this;
    }
    public override void OnJoinedRoom () {
        base.OnJoinedRoom ();
        Camera.main.gameObject.SetActive (false);
        if (PunUserNetControl.LocalPlayerInstance == null) {
            Debug.Log ("We are Instantiating LocalPlayer from " + SceneManagerHelper.ActiveSceneName);
            PunNetworkManager.singleton.SpawnPlayer ();
        }
        else {
            Debug.Log ("Ignoring scene load for " + SceneManagerHelper.ActiveSceneName);
        }
    }
    public void SpawnPlayer () {
        // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
        PhotonNetwork.Instantiate (GamePlayerPrefab.name,
            new Vector3 (0f, 5f, 0f), Quaternion.identity, 0);
    }
}